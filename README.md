# APL
 Dr. W gave me an old program of his... Can you tell me what it does?
 
### Walkthrough
- By doing a google search of the challenge name, the programming language APL appears.
- Find the online compiler tryapl.org, paste in one line of code at a time.
- Reading the final output gives a string of decimal characters.
- Converting this string from decimal to ascii gives the flag.
